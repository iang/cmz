#![allow(non_snake_case)]

use curve25519_dalek::ristretto::RistrettoPoint;
use curve25519_dalek::scalar::Scalar;

use cmz::ggm::*;

#[test]
fn create_issuer() {
    let i0 = Issuer::new(0);
    println!("i0 = {:?}", i0);
    let i1 = Issuer::new(1);
    println!("i1 = {:?}", i1);
    let i5 = Issuer::new(5);
    println!("i5 = {:?}", i5);
}

#[test]
fn generator_test() {
    use hex_fmt::HexFmt;

    let A: &RistrettoPoint = &CMZ_A;
    let B: &RistrettoPoint = &CMZ_B;
    let two = Scalar::one() + Scalar::one();
    println!("A = {}", HexFmt(A.compress().to_bytes()));
    println!("B = {}", HexFmt(B.compress().to_bytes()));
    println!("2*A = {}", HexFmt((two * A).compress().to_bytes()));
    println!("2*A = {}", HexFmt((A + A).compress().to_bytes()));
    println!("2*B = {}", HexFmt((two * B).compress().to_bytes()));
    println!("2*B = {}", HexFmt((B + B).compress().to_bytes()));
}

#[test]
fn nonblind_5_test() {
    let mut rng: rand::rngs::ThreadRng = rand::thread_rng();
    let issuer = Issuer::new(5);
    let m1 = Scalar::random(&mut rng);
    let m2 = Scalar::random(&mut rng);
    let m3 = Scalar::random(&mut rng);
    let m4 = Scalar::random(&mut rng);
    let m5 = Scalar::random(&mut rng);
    let (req, state) = issue_nonblind_5::request(&m1, &m2, &m3, &m4, &m5);
    let resp = issuer.issue_nonblind_5(req);
    let result = issue_nonblind_5::verify(state, resp, &issuer.pubkey);
    assert!(result.is_ok());
}

#[test]
fn blind124_5_test() {
    let mut rng: rand::rngs::ThreadRng = rand::thread_rng();
    let issuer = Issuer::new(5);
    let m1 = Scalar::random(&mut rng);
    let m2 = Scalar::random(&mut rng);
    let m3 = Scalar::random(&mut rng);
    let m4 = Scalar::random(&mut rng);
    let m5 = Scalar::random(&mut rng);
    let (req, state) = issue_blind124_5::request(&m1, &m2, &m3, &m4, &m5);
    let resp = issuer.issue_blind124_5(req);
    assert!(resp.is_ok());
    let result = issue_blind124_5::verify(state, resp.unwrap(), &issuer.pubkey);
    assert!(result.is_ok());
}

#[test]
fn show_blind345_5_test() {
    let mut rng: rand::rngs::ThreadRng = rand::thread_rng();
    let issuer = Issuer::new(5);
    let m1 = Scalar::random(&mut rng);
    let m2 = Scalar::random(&mut rng);
    let m3 = Scalar::random(&mut rng);
    let m4 = Scalar::random(&mut rng);
    let m5 = Scalar::random(&mut rng);
    let (req, state) = issue_blind124_5::request(&m1, &m2, &m3, &m4, &m5);
    let resp = issuer.issue_blind124_5(req);
    assert!(resp.is_ok());
    let result = issue_blind124_5::verify(state, resp.unwrap(), &issuer.pubkey);
    assert!(result.is_ok());
    let cred = result.unwrap();
    let showmsg = show_blind345_5::show(&cred, &issuer.pubkey);
    let showresult = issuer.verify_blind345_5(showmsg);
    assert!(showresult.is_ok());
    let verifiedcred = showresult.unwrap();
    println!("Received credential: {:?}", verifiedcred);
}
